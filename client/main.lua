local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData                = {}
local GUI                       = {}
local HasAlreadyEnteredMarker   = false
local isDoingAction = false
local LastStation               = nil
local LastPart                  = nil
local LastPartNum               = nil
local CurrentAction             = nil
local CurrentActionMsg          = ''
local CurrentActionData         = {}
local moonshinerBlips = {}

ESX                             = nil
GUI.Time                        = 0


Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end

  while ESX.GetPlayerData() == nil do
    Citizen.Wait(10)
  end
  PlayerData = ESX.GetPlayerData()
end)



function OpenArmoryMenu(station)

  if Config.EnableArmoryManagement then

    local elements = {
      {label = _U('get_weapon'), value = 'get_weapon'},
      {label = _U('put_weapon'), value = 'put_weapon'},
      {label = 'Get Stock',  value = 'get_stock'},
      {label = 'Put Stock',  value = 'put_stock'}
    }

    --[[if PlayerData.job.grade_name == 'boss' then
      table.insert(elements, {label = _U('buy_weapons'), value = 'buy_weapons'})
    end]]

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory',
      {
        title    = _U('armory'),
        align    = 'bottom-right',
        elements = elements,
      },
      function(data, menu)

        if data.current.value == 'get_weapon' then
          OpenGetWeaponMenu()
        end

        if data.current.value == 'put_weapon' then
          OpenPutWeaponMenu()
        end

        --[[if data.current.value == 'buy_weapons' then
          OpenBuyWeaponsMenu(station)
        end]]

        if data.current.value == 'put_stock' then
              OpenPutStocksMenu()
            end

            if data.current.value == 'get_stock' then
              OpenGetStocksMenu()
            end

      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_armory'
        CurrentActionMsg  = _U('open_armory')
        CurrentActionData = {station = station}
      end
    )

  else

    local elements = {}

    for i=1, #Config.MoonshinerStations[station].AuthorizedWeapons, 1 do
      local weapon = Config.MoonshinerStations[station].AuthorizedWeapons[i]
      table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
    end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory',
      {
        title    = _U('armory'),
        align    = 'bottom-right',
        elements = elements,
      },
      function(data, menu)
        local weapon = data.current.value
        TriggerServerEvent('esx_moonshinerjob:giveWeapon', weapon,  1000)
      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_armory'
        CurrentActionMsg  = _U('open_armory')
        CurrentActionData = {station = station}

      end
    )

  end

end


function OpenVehicleSpawnerMenu(station, partNum)

  ESX.UI.Menu.CloseAll()

  if Config.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {
          label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']',
          value = garageVehicles[i]
        })
      end

      ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _U('vehicle_menu'),
        align    = 'bottom-right',
        elements = elements
      }, function(data, menu)
        menu.close()

        local vehicleProps = data.current.value
        local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

        if foundSpawnPoint then
          ESX.Game.SpawnVehicle(vehicleProps.model, spawnPoint, spawnPoint.heading, function(vehicle)
            ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
            TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
            exports["LegacyFuel"]:SetFuel(vehicle, 100)
            TriggerServerEvent('esx_vehiclelock:registerVehicleOwner', vehicle)
          end)

          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'moonshiner', vehicleProps)
        end
      end, function(data, menu)
        menu.close()

        CurrentAction     = 'menu_vehicle_spawner'
        CurrentActionMsg  = _U('vehicle_spawner')
        CurrentActionData = {station = station, partNum = partNum}
      end)

    end, 'moonshiner')

  else

    local elements = {}

    local sharedVehicles = Config.MoonshinerStations[station].AuthorizedVehicles.Shared
    for i=1, #sharedVehicles, 1 do
    table.insert(elements, { label = sharedVehicles[i].label, model = sharedVehicles[i].model})
  end

  --[[local authorizedVehicles = Config.MoonshinerStations[station].AuthorizedVehicles[PlayerData.job.grade_name]
  for i=1, #authorizedVehicles, 1 do
    table.insert(elements, { label = authorizedVehicles[i].label, model = authorizedVehicles[i].model})
  end]]

  ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
  {
    title    = _U('vehicle_menu'),
    align    = 'bottom-right',
    elements = elements
  }, function(data, menu)
    menu.close()

    local foundSpawnPoint, spawnPoint = GetAvailableVehicleSpawnPoint(station, partNum)

    if foundSpawnPoint then
      if Config.MaxInService == -1 then
        ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
          TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
          exports["LegacyFuel"]:SetFuel(vehicle, 100)
          TriggerServerEvent('esx_vehiclelock:registerVehicleOwner', vehicle)
          SetVehicleMaxMods(vehicle)
          SetVehicleWindowTint(vehicle, 0)
        end)
      else

        ESX.TriggerServerCallback('esx_service:isInService', function(isInService)

          if isInService then
            ESX.Game.SpawnVehicle(data.current.model, spawnPoint, spawnPoint.heading, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
              exports["LegacyFuel"]:SetFuel(vehicle, 100)
              TriggerServerEvent('esx_vehiclelock:registerVehicleOwner', vehicle)
              SetVehicleMaxMods(vehicle)
              SetVehicleWindowTint(vehicle, 0)
            end)
          else
            ESX.ShowNotification(_U('service_not'))
          end

        end, 'moonshiner')
      end
    end

  end, function(data, menu)
    menu.close()

    CurrentAction     = 'menu_vehicle_spawner'
    CurrentActionMsg  = _U('vehicle_spawner')
    CurrentActionData = {station = station, partNum = partNum}
  end)

end
end

function GetAvailableVehicleSpawnPoint(station, partNum)
  local spawnPoints = Config.MoonshinerStations[station].Vehicles[partNum].SpawnPoints
  local found, foundSpawnPoint = false, nil

  for i=1, #spawnPoints, 1 do
    if ESX.Game.IsSpawnPointClear(spawnPoints[i], spawnPoints[i].radius) then
      found, foundSpawnPoint = true, spawnPoints[i]
      break
    end
  end

  if found then
    return true, foundSpawnPoint
  else
    ESX.ShowNotification(_U('vehicle_blocked'))
    return false
  end
end


function OpenGetWeaponMenu()

  ESX.TriggerServerCallback('esx_moonshinerjob:getArmoryWeapons', function(weapons)

    local elements = {}

    for i=1, #weapons, 1 do
      if weapons[i].count > 0 then
        table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory_get_weapon',
      {
        title    = _U('get_weapon_menu'),
        align    = 'bottom-right',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        ESX.TriggerServerCallback('esx_moonshinerjob:removeArmoryWeapon', function()
          OpenGetWeaponMenu()
        end, data.current.value)

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutWeaponMenu()

  local elements   = {}
  local playerPed  = GetPlayerPed(-1)
  local weaponList = ESX.GetWeaponList()

  for i=1, #weaponList, 1 do

    local weaponHash = GetHashKey(weaponList[i].name)

    if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
      local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
      table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
    end

  end

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'armory_put_weapon',
    {
      title    = _U('put_weapon_menu'),
      align    = 'bottom-right',
      elements = elements,
    },
    function(data, menu)

      menu.close()

      ESX.TriggerServerCallback('esx_moonshinerjob:addArmoryWeapon', function()
        OpenPutWeaponMenu()
      end, data.current.value)

    end,
    function(data, menu)
      menu.close()
    end
  )

end

function OpenBuyWeaponsMenu(station)

  ESX.TriggerServerCallback('esx_moonshinerjob:getArmoryWeapons', function(weapons)

    local elements = {}

    for i=1, #Config.MoonshinerStations[station].AuthorizedWeapons, 1 do

      local weapon = Config.MoonshinerStations[station].AuthorizedWeapons[i]
      local count  = 0

      for i=1, #weapons, 1 do
        if weapons[i].name == weapon.name then
          count = weapons[i].count
          break
        end
      end

      table.insert(elements, {label = 'x' .. count .. ' ' .. ESX.GetWeaponLabel(weapon.name) .. ' $' .. weapon.price, value = weapon.name, price = weapon.price})

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory_buy_weapons',
      {
        title    = _U('buy_weapon_menu'),
        align    = 'bottom-right',
        elements = elements,
      },
      function(data, menu)

        ESX.TriggerServerCallback('esx_moonshinerjob:buy', function(hasEnoughMoney)

          if hasEnoughMoney then
            ESX.TriggerServerCallback('esx_moonshinerjob:addArmoryWeapon', function()
              OpenBuyWeaponsMenu(station)
            end, data.current.value)
          else
            ESX.ShowNotification(_U('not_enough_money'))
          end

        end, data.current.price)

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_moonshinerjob:getStockItems', function(items)

    print(json.encode(items))

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('moonshiner_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_moonshinerjob:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

  ESX.TriggerServerCallback('esx_moonshinerjob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_moonshinerjob:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

--[[
function OpenMoonshinerActionsMenu()
  ESX.UI.Menu.CloseAll()

  ESX.TriggerServerCallback('esx_moonshinerjob:hasStillEquipment', function(hasItem)
    if hasItem then
      local elements = {}

      ESX.TriggerServerCallback('esx_moonshinerjob:canMakeNormalMoonshine', function(canMake, elements)
        if canMake then
          table.insert(elements, {label = 'Normal Moonshine', value = 'normal_moonshine'}) -- elements is nil here so it will never insert into our table outside this scope
        end
      end, elements)
      ESX.TriggerServerCallback('esx_moonshinerjob:canMakeAppleMoonshine', function(canMake)
        if canMake then
          table.insert(elements, {label = 'Apple Moonshine', value = 'apple_moonshine'})
        end
      end)
      ESX.TriggerServerCallback('esx_moonshinerjob:canMakePeachMoonshine', function(canMake)
        if canMake then
          table.insert(elements, {label = 'Peach Moonshine', value = 'peach_moonshine'})
        end
      end)

      ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Moonshine_Menu',
      {
        title    = 'Make Moonshine',
        align    = 'bottom-right',
        elements = elements
      }, function(data, menu)
        if data.current.value == 'normal_moonshine' then
          menu.close()
          makeMoonshineStart('normal')
        elseif data.current.value == 'peach_moonshine' then
          menu.close()
          makeMoonshineStart('peach')
        elseif data.current.value == 'apple_moonshine' then
          menu.close()
          makeMoonshineStart('apple')
        end
      end, function(data, menu)
        menu.close()
      end)
    end
  end)
end
]]


function OpenMoonshinerActionsMenu()
  ESX.UI.Menu.CloseAll()

  ESX.TriggerServerCallback('esx_moonshinerjob:hasStillEquipment', function(hasItem)
    if hasItem then
      ESX.TriggerServerCallback('esx_moonshinerjob:canMakeNormalMoonshine', function(canMake)
        if canMake then
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'Moonshine_Menu_Normal',
            {
              title    = 'Make Moonshine',
              align    = 'bottom-right',
              elements = {{label = 'Normal Moonshine', value = 'normal_moonshine'}}
            }, function(data, menu)
              if data.current.value == 'normal_moonshine' then
                menu.close()
                makeMoonshineStart('normal')
              end
            end, function(data, menu)
            menu.close()
          end)
        else
          ESX.ShowNotification(_U('no_ingredients','Moonshine'))
        end
      end)

      ESX.TriggerServerCallback('esx_moonshinerjob:canMakePeachMoonshine', function(canMake)
        if canMake then
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'Moonshine_Menu_Peach',
            {
              title    = 'Make Moonshine',
              align    = 'bottom-right',
              elements = {{label = 'Peach Moonshine', value = 'peach_moonshine'}}
            }, function(data, menu)
              if data.current.value == 'peach_moonshine' then
                menu.close()
                makeMoonshineStart('peach')
              end
            end, function(data, menu)
            menu.close()
          end)
        else
          ESX.ShowNotification(_U('no_ingredients','Moonshine'))
        end
      end)

      ESX.TriggerServerCallback('esx_moonshinerjob:canMakeAppleMoonshine', function(canMake)
        if canMake then
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'Moonshine_Menu_Apple',
            {
              title    = 'Make Moonshine',
              align    = 'bottom-right',
              elements = {{label = 'Apple Moonshine', value = 'apple_moonshine'}}
            }, function(data, menu)
              if data.current.value == 'apple_moonshine' then
                menu.close()
                makeMoonshineStart('apple')
              end
            end, function(data, menu)
            menu.close()
          end)
        else
          ESX.ShowNotification(_U('no_ingredients','Moonshine'))
        end
      end)
    else
      ESX.ShowNotification(_U('no_still_equip','Moonshine Still Equipment'))
    end
  end)
end


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
  refreshBlips()
  refreshGroups()
end)


RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
  refreshBlips()
  refreshGroups()
end)


AddEventHandler('esx_moonshinerjob:hasEnteredMarker', function(station, part, partNum)
  local playerPed = GetPlayerPed(-1)

  if part == 'Armory' then
    CurrentAction     = 'menu_armory'
    CurrentActionMsg  = _U('open_armory')
    CurrentActionData = {station = station}
  elseif part == 'BossActions' then
    CurrentAction     = 'menu_boss_actions'
    CurrentActionMsg  = _U('open_bossmenu')
    CurrentActionData = {}
  elseif part == 'VehicleSpawner' then
    CurrentAction     = 'menu_vehicle_spawner'
    CurrentActionMsg  = _U('vehicle_spawner')
    CurrentActionData = {station = station, partNum = partNum}
  elseif part == 'StillSetup' then
    if IsPedInAnyVehicle(PlayerPedId(), false) then
      return
    end
    CurrentAction     = 'moonshine_menu'
    CurrentActionMsg  = _U('moonshine_menu', 'Moonshine Still Equipment')
    CurrentActionData = {station = station, partNum = partNum}
  elseif part == 'VehicleDeleter' then
    local coords    = GetEntityCoords(playerPed)

    if IsPedInAnyVehicle(playerPed,  false) then
      local vehicle = GetVehiclePedIsIn(playerPed, false)

      if DoesEntityExist(vehicle) then
        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('store_vehicle')
        CurrentActionData = {vehicle = vehicle}
      end
    end
  end
end)

AddEventHandler('esx_moonshinerjob:hasExitedMarker', function(station, part, partNum)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil
end)


function refreshGroups()
  local playerPed = GetPlayerPed(-1)
  SetPedRelationshipGroupHash(playerPed, GetHashKey('AMBIENT_GANG_HILLBILLY'))
end

function drawBlip(coords, icon, text, shortRange)
  local blip = AddBlipForCoord(coords.x, coords.y, coords.z)

  SetBlipSprite (blip, icon)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 0.9)
  SetBlipColour (blip, 1)
  SetBlipAsShortRange(blip, shortRange)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString(text)
  EndTextCommandSetBlipName(blip)
  table.insert(moonshinerBlips, blip)
end

function refreshBlips()
	deleteBlips()

	if PlayerData.job.name ~= nil and PlayerData.job.name == 'moonshiner' then
    drawBlip(Config.Dealers.DrugDealer_Moonshiner.coords, Config.Dealers.DrugDealer_Moonshiner.sprite, "Drug Trader", true)

    for k,v in pairs(Config.MoonshinerStations) do
  		drawBlip(v.Blip.Pos, v.Blip.Sprite, "Moonshiner House", true)

      for i=1, #v.Armories, 1 do
        drawBlip(v.Armories[i], 110, "Moonshiner Armory", true)
      end

      for i=1, #v.Vehicles, 1 do
        drawBlip(v.Vehicles[i].Spawner, 315, "Vehicle Spawner", true)
      end

      for i=1, #v.VehicleDeleters, 1 do
        drawBlip(v.VehicleDeleters[i], 315, "Vehicle Deleter", true)
      end

      for i=1, #v.StillSetupLocations, 1 do
        drawBlip(v.StillSetupLocations[i], 368, "Still Setup Locations", true)
      end

      if PlayerData.job.grade_name == 'boss' then
        drawBlip(v.Blip.Pos, 521, "Moonshiner Boss Menu", true)
      end
    end
	end
end

function deleteBlips()
  if moonshinerBlips[1] ~= nil then
    for i = 1, #moonshinerBlips, 1 do
      RemoveBlip(moonshinerBlips[i])
      moonshinerBlips[i] = nil
    end
  end
end

--Action Blocking
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if isDoingAction then
      DisableControlAction(0, Keys['F1'], true) -- Disable phone
      DisableControlAction(0, Keys['F2'], true) -- Inventory
      DisableControlAction(0, Keys['F3'], true) -- Animations
      DisableControlAction(0, Keys['F6'], true) -- Job
      DisableControlAction(0, Keys['Enter'], true)
      DisableControlAction(0, Keys['E'], true)
    else
      Citizen.Wait(500)
    end
  end
end)

function makeMoonshineStart(moonshineType)
  Wait(10)
  ESX.UI.Menu.CloseAll()

  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  local oldCoords = GetEntityCoords(PlayerPedId())
  local duration = 60
  local timer = 0
  local failed = false

  while IsPedUsingScenario(PlayerPedId(),scenario) == false do
    Wait(0)
  end

  ESX.ShowNotification(_U('action_start'))
  ESX.ShowNotification('Estimated Time: 60s')

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(PlayerPedId())
    local isDead = IsEntityDead(PlayerPedId())
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(PlayerPedId(),scenario) == false

    if isDead or movedAway or cancelledAnim then
      isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      ClearPedTasksImmediately(PlayerPedId())
      failed = true
      break
    end

    if timer == duration then
      isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())

      if moonshineType == 'normal' then
        TriggerServerEvent('esx_moonshinerjob:makeNormalMoonshine')
      elseif moonshineType == 'apple' then
        TriggerServerEvent('esx_moonshinerjob:makeAppleMoonshine')
      elseif moonshineType == 'peach' then
        TriggerServerEvent('esx_moonshinerjob:makePeachMoonshine')
      end

      break
    end

    Wait(1000)
  end
end

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    if PlayerData.job ~= nil then
      --print("not nil")
      if PlayerData.job.name == 'moonshiner' then
        --print("moonshiner")
        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        for k,v in pairs(Config.MoonshinerStations) do
          for i=1, #v.Armories, 1 do
            --print("1")
            if GetDistanceBetweenCoords(coords,  v.Armories[i].x,  v.Armories[i].y,  v.Armories[i].z,  true) < Config.DrawDistance then
              --print("2")
              DrawMarker(Config.MarkerType, v.Armories[i].x, v.Armories[i].y, v.Armories[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end

          for i=1, #v.Vehicles, 1 do
						if not IsPedInAnyVehicle(PlayerPedId(), false) and GetDistanceBetweenCoords(coords, v.Vehicles[i].Spawner.x, v.Vehicles[i].Spawner.y, v.Vehicles[i].Spawner.z, true) < Config.DrawDistance then
						DrawMarker(Config.MarkerType, v.Vehicles[i].Spawner.x, v.Vehicles[i].Spawner.y, v.Vehicles[i].Spawner.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
						end
					end

					for i=1, #v.VehicleDeleters, 1 do
						if IsPedInAnyVehicle(PlayerPedId(), false) and GetDistanceBetweenCoords(coords, v.VehicleDeleters[i].x, v.VehicleDeleters[i].y, v.VehicleDeleters[i].z, true) < Config.DrawDistance then
						DrawMarker(Config.MarkerType, v.VehicleDeleters[i].x, v.VehicleDeleters[i].y, v.VehicleDeleters[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSizeL.x, Config.MarkerSizeL.y, Config.MarkerSizeL.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
						end
					end

          for i=1, #v.StillSetupLocations, 1 do
						if not IsPedInAnyVehicle(PlayerPedId(), false) and GetDistanceBetweenCoords(coords, v.StillSetupLocations[i].x, v.StillSetupLocations[i].y, v.StillSetupLocations[i].z, true) < Config.DrawDistance then
						DrawMarker(Config.MarkerType, v.StillSetupLocations[i].x, v.StillSetupLocations[i].y, v.StillSetupLocations[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
						end
					end

          if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.name == 'moonshiner' and PlayerData.job.grade_name == 'boss' then
            for i=1, #v.BossActions, 1 do
              if not v.BossActions[i].disabled and GetDistanceBetweenCoords(coords,  v.BossActions[i].x,  v.BossActions[i].y,  v.BossActions[i].z,  true) < Config.DrawDistance then
                DrawMarker(Config.MarkerType, v.BossActions[i].x, v.BossActions[i].y, v.BossActions[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
              end
            end
          end
        end
      else
        Citizen.Wait(500)
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Wait(0)

    if PlayerData.job ~= nil then
      if PlayerData.job.name == 'moonshiner' then
        local playerPed      = GetPlayerPed(-1)
        local coords         = GetEntityCoords(playerPed)
        local isInMarker     = false
        local currentStation = nil
        local currentPart    = nil
        local currentPartNum = nil

        for k,v in pairs(Config.MoonshinerStations) do
          for i=1, #v.Armories, 1 do
            if GetDistanceBetweenCoords(coords,  v.Armories[i].x,  v.Armories[i].y,  v.Armories[i].z,  true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'Armory'
              currentPartNum = i
            end
          end

          for i=1, #v.Vehicles, 1 do
            if GetDistanceBetweenCoords(coords, v.Vehicles[i].Spawner.x, v.Vehicles[i].Spawner.y, v.Vehicles[i].Spawner.z, true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'VehicleSpawner'
              currentPartNum = i
            end
          end

          for i=1, #v.VehicleDeleters, 1 do
            if GetDistanceBetweenCoords(coords, v.VehicleDeleters[i].x, v.VehicleDeleters[i].y, v.VehicleDeleters[i].z, true) < Config.MarkerSizeL.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'VehicleDeleter'
              currentPartNum = i
            end
          end

          for i=1, #v.StillSetupLocations, 1 do
            if GetDistanceBetweenCoords(coords, v.StillSetupLocations[i].x, v.StillSetupLocations[i].y, v.StillSetupLocations[i].z, true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'StillSetup'
              currentPartNum = i
            end
          end

          if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.name == 'moonshiner' and PlayerData.job.grade_name == 'boss' then
            for i=1, #v.BossActions, 1 do
              if GetDistanceBetweenCoords(coords,  v.BossActions[i].x,  v.BossActions[i].y,  v.BossActions[i].z,  true) < Config.MarkerSize.x then
                isInMarker     = true
                currentStation = k
                currentPart    = 'BossActions'
                currentPartNum = i
              end
            end
          end
        end

        local hasExited = false

        if isInMarker and not HasAlreadyEnteredMarker or (isInMarker and (LastStation ~= currentStation or LastPart ~= currentPart or LastPartNum ~= currentPartNum) ) then
          if
            (LastStation ~= nil and LastPart ~= nil and LastPartNum ~= nil) and
            (LastStation ~= currentStation or LastPart ~= currentPart or LastPartNum ~= currentPartNum)
          then
            TriggerEvent('esx_moonshinerjob:hasExitedMarker', LastStation, LastPart, LastPartNum)
            hasExited = true
          end

          HasAlreadyEnteredMarker = true
          LastStation             = currentStation
          LastPart                = currentPart
          LastPartNum             = currentPartNum

          TriggerEvent('esx_moonshinerjob:hasEnteredMarker', currentStation, currentPart, currentPartNum)
        end

        if not hasExited and not isInMarker and HasAlreadyEnteredMarker then
          HasAlreadyEnteredMarker = false
          TriggerEvent('esx_moonshinerjob:hasExitedMarker', LastStation, LastPart, LastPartNum)
        end
      else
        Citizen.Wait(500)
      end
    end
  end
end)


-- Key Controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    if CurrentAction ~= nil then
      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlPressed(0,  Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'moonshiner' and (GetGameTimer() - GUI.Time) > 150 then
        if CurrentAction == 'menu_armory' then
          OpenArmoryMenu(CurrentActionData.station)
        elseif CurrentAction == 'moonshine_menu' then
          OpenMoonshinerActionsMenu()
        elseif CurrentAction == 'menu_vehicle_spawner' then
          OpenVehicleSpawnerMenu(CurrentActionData.station, CurrentActionData.partNum)
        elseif CurrentAction == 'delete_vehicle' then
          if Config.EnableSocietyOwnedVehicles then
            local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
            TriggerServerEvent('esx_society:putVehicleInGarage', 'moonshiner', vehicleProps)
          end
          ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
        elseif CurrentAction == 'menu_boss_actions' then
          ESX.UI.Menu.CloseAll()

          TriggerEvent('esx_society:openBossMenu', 'moonshiner', function(data, menu)
            menu.close()
            CurrentAction     = 'menu_boss_actions'
            CurrentActionMsg  = _U('open_bossmenu')
            CurrentActionData = {}
          end)

        end

        CurrentAction = nil
        GUI.Time      = GetGameTimer()
      end
    end
  end
end)
