Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerSizeL                 = { x = 3.0, y = 3.0, z = 1.0 }
Config.MarkerColor                = { r = 204, g = 50, b = 50 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = true
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale                     = 'en'

Config.Dealers = {
	DrugDealer_Moonshiner = {coords = vector3(2232.40, 5611.67, 53.91), name = _U('blip_drugdealer'), color = 6, sprite = 355, radius = 25.0},
}

Config.MoonshinerStations = {
  Moonshiner = {
    Blip = {
      Pos     = { x = -1149.70, y = 4940.43, z = 221.27 },
      Sprite  = 78,
      Display = 4,
      Scale   = 1.2,
      Colour  = 1
    },

    Armories = {
      { x = -1137.68, y = 4940.34, z = 221.27 }
    },

    BossActions = {
      { x = -1149.70, y = 4940.43, z = 221.27 }
    },

		Vehicles = {
			{
				Spawner    = { x = -1131.85, y = 4915.22, z = 218.79 },
				SpawnPoints = {
					{ x = -1132.18, y = 4910.13, z = 218.50, heading = 34.89, radius = 6.0 }
				}
			}
		},

		VehicleDeleters = {
			{ x = -1129.85, y = 4920.16, z = 218.49 }
		},

		StillSetupLocations = {
			{ x = -1430.72, y = 4316.92, z = 0.58 },
			{ x = -1549.31, y = 4340.29, z = 1.08 },
			{ x = -1820.33, y = 4566.85, z = 0.64 },
			{ x = -1253.80, y = 4415.91, z = 5.80 },
			{ x = -875.20, y = 4434.62, z = 14.88 },
			{ x = -259.25, y = 4273.55, z = 30.87 },
		}
  }
}
