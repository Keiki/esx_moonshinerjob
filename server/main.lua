ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'moonshiner', Config.MaxInService)
end

-- TriggerEvent('esx_phone:registerNumber', 'moonshiner', _U('alert_moonshiner'), true, true)
TriggerEvent('esx_society:registerSociety', 'moonshiner', 'Moonshiner', 'society_moonshiner', 'society_moonshiner', 'society_moonshiner', {type = 'public'})


RegisterServerEvent('esx_moonshinerjob:giveWeapon')
AddEventHandler('esx_moonshinerjob:giveWeapon', function(weapon, ammo)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.addWeapon(weapon, ammo)
end)

RegisterServerEvent('esx_moonshinerjob:getStockItem')
AddEventHandler('esx_moonshinerjob:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_moonshiner', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

  end)

end)


ESX.RegisterServerCallback('esx_moonshinerjob:hasStillEquipment', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local xItem = xPlayer.getInventoryItem('still_equip')

  if xItem.count > 0 then
    cb(true)
  else
    TriggerClientEvent('esx:showNotification', source, _U('no_still_equip', 1, xItem.label))
    cb(false)
  end
end)


ESX.RegisterServerCallback('esx_moonshinerjob:canMakeNormalMoonshine', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local xItem_Barley = xPlayer.getInventoryItem('barley')
  local xItem_Corn = xPlayer.getInventoryItem('corn')
  local xItem_Rye = xPlayer.getInventoryItem('rye')

  if xItem_Barley.count > 0 or xItem_Corn.count > 0 or xItem_Rye.count > 0 then
    cb(true, elements)
  else
    cb(false, elements)
  end
end)

RegisterServerEvent('esx_moonshinerjob:makeNormalMoonshine')
AddEventHandler('esx_moonshinerjob:makeNormalMoonshine', function()
  local xPlayer = ESX.GetPlayerFromId(source)
  local xItem_Barley = xPlayer.getInventoryItem('barley')
  local xItem_Corn = xPlayer.getInventoryItem('corn')
  local xItem_Rye = xPlayer.getInventoryItem('rye')
  local xItem_NormalMoonshine = xPlayer.getInventoryItem('moonshine_normal')

  if xItem_Barley.count > 0 then
    xPlayer.removeInventoryItem('barley', 1)
  elseif xItem_Corn.count > 0 then
    xPlayer.removeInventoryItem('corn', 1)
  elseif xItem_Rye.count > 0 then
    xPlayer.removeInventoryItem('rye', 1)
  end

  xPlayer.addInventoryItem('moonshine_normal', 1)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
end)


ESX.RegisterServerCallback('esx_moonshinerjob:canMakeAppleMoonshine', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local xItem_Barley = xPlayer.getInventoryItem('barley')
  local xItem_Corn = xPlayer.getInventoryItem('corn')
  local xItem_Rye = xPlayer.getInventoryItem('rye')
  local xItem_Apples = xPlayer.getInventoryItem('apples')

  if (xItem_Barley.count > 0 or xItem_Corn.count > 0 or xItem_Rye.count > 0) and xItem_Apples.count > 0 then
    cb(true)
  else
    cb(false)
  end
end)

RegisterServerEvent('esx_moonshinerjob:makeAppleMoonshine')
AddEventHandler('esx_moonshinerjob:makeAppleMoonshine', function()
  local xPlayer = ESX.GetPlayerFromId(source)
  local xItem_Barley = xPlayer.getInventoryItem('barley')
  local xItem_Corn = xPlayer.getInventoryItem('corn')
  local xItem_Rye = xPlayer.getInventoryItem('rye')
  local xItem_Apples = xPlayer.getInventoryItem('apples')
  local xItem_AppleMoonshine = xPlayer.getInventoryItem('moonshine_apple')

  if xItem_Barley.count > 0 then
    xPlayer.removeInventoryItem('barley', 1)
  elseif xItem_Corn.count > 0 then
    xPlayer.removeInventoryItem('corn', 1)
  elseif xItem_Rye.count > 0 then
    xPlayer.removeInventoryItem('rye', 1)
  end

  xPlayer.removeInventoryItem('apples', 1)
  xPlayer.addInventoryItem('moonshine_apple', 1)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
end)


ESX.RegisterServerCallback('esx_moonshinerjob:canMakePeachMoonshine', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local xItem_Barley = xPlayer.getInventoryItem('barley')
  local xItem_Corn = xPlayer.getInventoryItem('corn')
  local xItem_Rye = xPlayer.getInventoryItem('rye')
  local xItem_Peaches = xPlayer.getInventoryItem('peaches')

  if (xItem_Barley.count > 0 or xItem_Corn.count > 0 or xItem_Rye.count > 0) and xItem_Peaches.count > 0 then
    cb(true)
  else
    cb(false)
  end
end)

RegisterServerEvent('esx_moonshinerjob:makePeachMoonshine')
AddEventHandler('esx_moonshinerjob:makePeachMoonshine', function()
  local xPlayer = ESX.GetPlayerFromId(source)
  local xItem_Barley = xPlayer.getInventoryItem('barley')
  local xItem_Corn = xPlayer.getInventoryItem('corn')
  local xItem_Rye = xPlayer.getInventoryItem('rye')
  local xItem_Peaches = xPlayer.getInventoryItem('peaches')
  local xItem_PeachMoonshine = xPlayer.getInventoryItem('moonshine_peach')

  if xItem_Barley.count > 0 then
    xPlayer.removeInventoryItem('barley', 1)
  elseif xItem_Corn.count > 0 then
    xPlayer.removeInventoryItem('corn', 1)
  elseif xItem_Rye.count > 0 then
    xPlayer.removeInventoryItem('rye', 1)
  end

  xPlayer.removeInventoryItem('peaches', 1)
  xPlayer.addInventoryItem('moonshine_peach', 1)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
end)


RegisterServerEvent('esx_moonshinerjob:putStockItems')
AddEventHandler('esx_moonshinerjob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_moonshiner', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

  end)

end)



ESX.RegisterServerCallback('esx_moonshinerjob:getArmoryWeapons', function(source, cb)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_moonshiner', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    cb(weapons)

  end)

end)

ESX.RegisterServerCallback('esx_moonshinerjob:addArmoryWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeWeapon(weaponName)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_moonshiner', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_moonshinerjob:removeArmoryWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.addWeapon(weaponName, 1000)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_moonshiner', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)


ESX.RegisterServerCallback('esx_moonshinerjob:buy', function(source, cb, amount)

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_moonshiner', function(account)

    if account.money >= amount then
      account.removeMoney(amount)
      cb(true)
    else
      cb(false)
    end

  end)

end)

ESX.RegisterServerCallback('esx_moonshinerjob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_moonshiner', function(inventory)
    cb(inventory.items)
  end)

end)

ESX.RegisterServerCallback('esx_moonshinerjob:getPlayerInventory', function(source, cb)

  local xPlayer = ESX.GetPlayerFromId(source)
  local items   = xPlayer.inventory

  cb({
    items = items
  })

end)
